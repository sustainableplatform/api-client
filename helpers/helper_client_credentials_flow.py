from requests_oauthlib import OAuth2Session
from oauthlib.oauth2 import BackendApplicationClient


def get_token_client_credentials_flow(client_id, client_secret, token_url):
    client = BackendApplicationClient(client_id=client_id)
    oauth = OAuth2Session(client=client)
    token = oauth.fetch_token(
        token_url=token_url,
        client_id=client_id,
        client_secret=client_secret
    )
    return token
