import json

def get_all_markets(sp_url, sp_client):
    market_endpoint = sp_url + '/api/markets/'
    market_result = sp_client.post(market_endpoint)
    market_list = json.loads(market_result.text)['markets'].keys()
    return market_list


def get_all_available_fields(sp_url, sp_client):
    fields_endpoint = sp_url + '/api/fields_available/'
    fields_list = sp_client.post(fields_endpoint).json()
    return fields_list['all_fields']


def get_market_ids(sp_url, sp_client, market):
    req_data = {
        'Market': market
    }
    market_id_endpoint = sp_url + '/api/market_ids/'
    this_market_ids = json.loads(sp_client.post(market_id_endpoint, data=req_data).text)
    ids = this_market_ids['market_ids']

    return ids

def multi_company(sp_url, sp_client, ids_of_company="1,2,3"):
    req_data = {'port_ids': ids_of_company}  # Max of 1000
    multi_data_url = sp_url + '/api/portfolio_data/'
    data_to_output = sp_client.post(multi_data_url, data=req_data)
    return data_to_output.json()
