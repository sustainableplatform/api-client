from requests_oauthlib import OAuth2Session


def get_auth_code_token(client_id, client_secret, authorization_base_url, token_url):

    sp = OAuth2Session(client_id)

    authorization_url, state = sp.authorization_url(
        authorization_base_url,
        access_type="offline"
    )

    print("Please go to %s and authorise access." % authorization_url)

    auth_response = input('Enter Callback URL: ')

    token = sp.fetch_token(
        token_url,
        authorization_response=auth_response,
        client_secret=client_secret
    )
    return token
