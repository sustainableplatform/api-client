import os
import pathlib

# Lockfile Commands


def make_lock_file(filename='lockfile'):
    if(lock_file_exists(filename)):
        print('Lockfile already exists')
        return False
    else:
        pathlib.Path('%s.lock' % filename).touch()
        return True


def lock_file_exists(filename='lockfile'):
    if os.path.isfile('%s.lock' % filename):
        return True
    return False


def delete_lock_file(filename='lockfile'):
    if(not lock_file_exists(filename)):
        print('Lockfile does not exists')
        return False

    os.remove('%s.lock' % filename)
    return True
