import json
import os
import time
import csv
from datetime import date

from requests_oauthlib import OAuth2Session
import flask
from flask import Flask, request, redirect, session, url_for, render_template
from flask import send_from_directory


from helpers import make_lock_file, delete_lock_file
from helpers import lock_file_exists
from helpers import get_token_client_credentials_flow

app = Flask(__name__,
            template_folder='templates',
            static_url_path='',
            )

with open('client_credentials.json', 'r') as json_file:
    data = json.load(json_file)
    client_id = data[0]["client_id"]
    client_secret = data[0]["client_secret"]

this_server_ip = "0.0.0.0"
secure = False

# grant_type = 'auth_code'
grant_type = 'client_credentials'

if secure:
    this_server_address = "https://%s" % this_server_ip
else:
    this_server_address = "http://%s" % this_server_ip

# sp_url = 'http://localhost:8002'  # SP Internal Testing
sp_url = 'https://api.sustainableplatform.com'

oauth_endpoint = sp_url + '/o/'
authorization_base_url = oauth_endpoint + 'authorize/'
token_url = oauth_endpoint + 'token/'


@app.route('/')
def hello():
    return render_template('index.html')


@app.route("/auth")
def authenticate():
    """Step 1: User Authorization.
    """
    if grant_type == 'auth_code':
        redirect_location = authenticate_auth_code()
    elif grant_type == 'client_credentials':
        redirect_location = authenticate_client_credentials()
    return redirect_location


def authenticate_auth_code():
    """
    Redirect the user/resource owner to Sustainable Platform
    using an URL with a few key OAuth parameters.

    After authentication, SP should redirect back to /callback
    """
    sp = OAuth2Session(client_id)
    authorization_url, state = sp.authorization_url(authorization_base_url)

    # State is used to prevent CSRF, keep this for later.
    session['oauth_state'] = state
    return redirect(authorization_url)


def authenticate_client_credentials():
    token = get_token_client_credentials_flow(client_id, client_secret, token_url)
    session['oauth_token'] = token
    return redirect(url_for('.search'))


# Step 2: User authorization, this happens on the provider.
@app.route("/callback", methods=["GET"])
def callback():
    """ Step 3: Retrieving an access token.

    The user has been redirected back from the provider to your registered
    callback URL. With this redirection comes an authorization code included
    in the redirect URL. We will use that to obtain an access token.
    """

    api_code = request.args.get('code')
    sp = OAuth2Session(client_id, state=session['oauth_state'])
    token = sp.fetch_token(
        token_url,
        code=api_code,
        client_secret=client_secret,
        authorization_response=this_server_address+':5000'
    )

    session['oauth_token'] = token

    return redirect(url_for('.search'))


@app.route("/search", methods=["GET", "POST"])
def search():
    """Fetching a protected resource using an OAuth 2 token.
    """

    # TODO: Automatically refresh an expired token.
    # TODO: Store token? (I don't think I need to, but we'll see.)

    try:
        sp = OAuth2Session(client_id, token=session['oauth_token'])
    except KeyError:
        return redirect(url_for('.error'))

    get = True
    if flask.request.method == 'POST':
        get = False

    if get:
        return render_template('search_page.html')

    api_req = sp_url + '/api/search/'

    data_inputted = dict(flask.request.form)
    keys_of_input = data_inputted.keys()

    if('symbol_input' in keys_of_input and 'market_input' in keys_of_input):
        req_data = {
            'symbol': data_inputted.get('symbol_input', ''),
            'market': data_inputted.get('market_input', '')
        }
    elif('isin_input' in keys_of_input):
        req_data = {
            'isin': data_inputted.get('isin_input', '')
        }
    elif('name_input' in keys_of_input):
        req_data = {
            'name': data_inputted.get('name_input', '')
        }
    else:
        req_data = {
            'name': ''
        }
        print('Something Didnt work')
        # breakpoint()

    search_result = sp.post(api_req, data=req_data)
    # print(search_result.text)
    return search_result.json()


@app.route("/date_search", methods=["GET", "POST"])
def date_search():

    try:
        sp = OAuth2Session(client_id, token=session['oauth_token'])
    except KeyError:
        return redirect(url_for('.error'))

    api_req = sp_url + '/api/search/date'

    data_inputted = dict(flask.request.form)
    keys_of_input = data_inputted.keys()

    req_data = dict()

    if('earliest_date' in keys_of_input):
        req_data['earliest_date'] = data_inputted['earliest_date']

    if('latest_date' in keys_of_input):
        req_data['latest_date'] = data_inputted['latest_date']

    companies_between_these_dates = sp.post(api_req, data=req_data)
    # print(port_data_to_ouput.text)
    return companies_between_these_dates.json()


@app.route("/isin_list", methods=["GET", "POST"])
def isin_list():

    try:
        sp = OAuth2Session(client_id, token=session['oauth_token'])
    except KeyError:
        return redirect(url_for('.error'))

    get = True
    if flask.request.method == 'POST':
        get = False

    if get:
        return render_template('isin_search.html')

    api_req = sp_url + '/api/search/isin'

    data_inputted = dict(flask.request.form)
    keys_of_input = data_inputted.keys()

    if('isins_wanted' in keys_of_input):
        req_data = {
            'isins_wanted': data_inputted['isins_wanted'],
        }

    all_isins_ids = sp.post(api_req, data=req_data)
    # print(port_data_to_ouput.text)
    return all_isins_ids.json()


@app.route("/fields_request", methods=["GET"])
def all_fields():
    try:
        sp = OAuth2Session(client_id, token=session['oauth_token'])
    except KeyError:
        return redirect(url_for('.error'))

    api_req = sp_url + '/api/fields_available/'
    data_to_ouput = sp.post(api_req)
    return data_to_ouput.json()


@app.route("/markets", methods=["GET"])
def all_markets():
    try:
        sp = OAuth2Session(client_id, token=session['oauth_token'])
    except KeyError:
        return redirect(url_for('.error'))

    api_req = sp_url + '/api/markets/'
    data_to_ouput = sp.post(api_req)
    return data_to_ouput.json()


@app.route("/market_ids", methods=["POST"])
def market():
    try:
        sp = OAuth2Session(client_id, token=session['oauth_token'])
    except KeyError:
        return redirect(url_for('.error'))

    data_inputted = dict(flask.request.form)

    req_data = {
        'Market': data_inputted['market_input'],
    }

    api_req = sp_url + '/api/market_ids/'

    data_to_ouput = sp.post(api_req, data=req_data)
    return data_to_ouput.json()


@app.route("/market_company_information", methods=["POST"])
def market_company_info():
    try:
        sp = OAuth2Session(client_id, token=session['oauth_token'])
    except KeyError:
        return redirect(url_for('.error'))

    data_inputted = dict(flask.request.form)

    req_data = {
        'Market': data_inputted['market_input'],
    }

    api_req = sp_url + '/api/market_company_information/'

    data_to_ouput = sp.post(api_req, data=req_data)
    return data_to_ouput.json()


@app.route("/data", methods=["GET", "POST"])
def data():

    try:
        sp = OAuth2Session(client_id, token=session['oauth_token'])
    except KeyError:
        return redirect(url_for('.error'))

    get = True
    if flask.request.method == 'POST':
        get = False

    if get:
        fields_api_endpoint = sp_url + '/api/fields_available/'
        field_json = json.loads(sp.post(fields_api_endpoint).text)
        list_of_fields = field_json['all_fields']

        all_fields = ''
        for field in list_of_fields:
            all_fields += '%s,\n' % field
        return render_template('data.html', fields=all_fields)

    api_req = sp_url + '/api/data/'

    data_inputted = dict(flask.request.form)
    keys_of_input = data_inputted.keys()

    if('company_id' in keys_of_input):
        req_data = {
            'company_id': data_inputted['company_id'],
        }

        fields = data_inputted['fields_required'].replace("  ", " ").strip()
        if(fields):
            req_data['fields'] = fields

    data_to_ouput = sp.post(api_req, data=req_data)
    # print(data_to_ouput.text)
    return data_to_ouput.json()


@app.route("/port_data", methods=["GET", "POST"])
def port_data():

    try:
        sp = OAuth2Session(client_id, token=session['oauth_token'])
    except KeyError:
        return redirect(url_for('.error'))

    get = True
    if flask.request.method == 'POST':
        get = False

    if get:
        return redirect(url_for('.data'))

    api_req = sp_url + '/api/portfolio_data/'

    data_inputted = dict(flask.request.form)
    keys_of_input = data_inputted.keys()

    if('port_id_list' in keys_of_input):
        req_data = {
            'port_ids': data_inputted['port_id_list'],
        }

        fields = data_inputted['fields_required'].replace("  ", " ").strip()
        if(fields):
            req_data['fields'] = fields

    port_data_to_ouput = sp.post(api_req, data=req_data)
    # print(port_data_to_ouput.text)
    return port_data_to_ouput.json()


# Example of how just to fetch data:
'''

Get token,
sp = OAuth2Session(client_id, token=<TOKEN>)

SINGLE COMPANY:
req_data = { 'company_id': "<COMPANY_ID>" }
data_url = sp_url + '/api/data/'
data_to_ouput = sp.post(data_url, data=req_data)
data_to_ouput.json()

MULTIPLE COMPANIES:
req_data = {'port_ids': "<COMPANY_ID>,<COMPANY_ID>,<COMPANY_ID>"} # Max of 1000
multi_data_url = sp_url + '/api/portfolio_data/'
data_to_ouput = sp.post(multi_data_url, data=req_data)
data_to_ouput.json()

'''


@app.route("/full_data_file", methods=["GET", "POST"])
def full_data_file():
    lockfile_name = 'full_db'

    try:
        sp = OAuth2Session(client_id, token=session['oauth_token'])
    except KeyError:
        return redirect(url_for('.error'))

    get = True
    if flask.request.method == 'POST':
        get = False

    if(get):
        generation_status = ''
        if(lock_file_exists(lockfile_name)):
            lockfile_file = open('%s.lock' % lockfile_name)
            generation_status = lockfile_file.read()
            lockfile_file.close()
        all_current_files = []
        for (dirpath, dirnames, filenames) in os.walk('generated_files'):
            all_current_files.extend(filenames)
        return render_template(
                'full_data.html',
                gen_status=generation_status,
                existing_files=all_current_files
            )

    if(not lock_file_exists(lockfile_name)):
        make_lock_file(lockfile_name)
        market_id_dict = market_ids_dictionary(sp)

        all_comp_data = []
        all_markets = [m for m in market_id_dict.keys()]
        for market in all_markets:
            list_of_companies = market_id_dict.get(market, [])
            market_this_data = market_all_data(sp, market, list_of_companies)
            all_comp_data.extend(market_this_data)
            print("## %s is done" % market)

        today = date.today()
        todays_date = today.strftime("%Y-%m-%d")
        file_name = 'generated_files/%s.csv' % todays_date
        api_req = sp_url + '/api/fields_available/'
        fieldnames = sp.post(api_req).json()["all_fields"]

        with open(file_name, 'w', newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

            writer.writeheader()
            for comp in all_comp_data:
                del comp['id']
                writer.writerow(comp)

        delete_lock_file(lockfile_name)

    return redirect(url_for('.full_data_file'))


@app.route("/error")
def error():
    return render_template('error.html')


@app.route('/js/<path:path>')
def send_js(path):
    # Static File Server
    return send_from_directory('static/js', path)


@app.route('/file/<path:path>')
def send_gen_file(path):
    return send_from_directory('generated_files', path)


def market_ids_dictionary(sp):
    api_req = sp_url + '/api/markets/'
    market_list = json.loads(sp.post(api_req).text)['markets'].keys()

    fetched_market_ids = {}
    for market in market_list:
        req_data = {
            'Market': market
        }

        api_req = sp_url + '/api/market_ids/'

        this_market_ids = json.loads(sp.post(api_req, data=req_data).text)
        ids = this_market_ids['market_ids']
        if(ids != []):
            # print(market, ids)
            fetched_market_ids[market] = ids
    return fetched_market_ids


def market_all_data(sp, market, list_of_companies):
    api_req = sp_url + '/api/portfolio_data/'

    market_company = []
    all_reqs = []
    chunk_size = 500
    number_of_loops = (len(list_of_companies) // chunk_size) + 1
    for i in range(0, number_of_loops):
        start_point = i*chunk_size
        end_point = (i+1)*chunk_size
        if(end_point >= len(list_of_companies)):
            ids_wanted = list_of_companies[start_point:]
        else:
            ids_wanted = list_of_companies[start_point:end_point]
        all_reqs.append(ids_wanted)

    for i, this_req in enumerate(all_reqs):
        req_num = "%s/%s" % (i+1, len(all_reqs))
        with open("full_db.lock", "w") as lockfile:
            lockfile.write('Doing Market: %s, Request: %s' % (market, req_num))

        this_req = ','.join([str(id) for id in this_req])

        print("Getting %s companies from %s" % (len(this_req), market))

        req_data = {
            'port_ids': this_req
        }
        this_lookup = sp.post(api_req, data=req_data).text
        this_lookup = json.loads(this_lookup)
        these_results = this_lookup["company_data"]

        market_company = market_company + these_results
        time.sleep(1)  # Sleep for 1 second.

    return market_company


if __name__ == "__main__":
    if(not secure):
        # This allows us to use a plain HTTP callback
        os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = "1"

    if not os.path.exists('generated_files'):
        os.makedirs('generated_files')

    delete_lock_file('full_db')

    app.secret_key = os.urandom(24)
    app.run(debug=True, host=this_server_ip)
