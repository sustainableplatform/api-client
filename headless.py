import csv
import json
import os
from datetime import date

from requests_oauthlib import OAuth2Session

from helpers import get_auth_code_token, get_token_client_credentials_flow


with open('client_credentials.json', 'r') as json_file:
    data = json.load(json_file)
    client_id = data[0]["client_id"]
    client_secret = data[0]["client_secret"]

sp_url = 'https://api.sustainableplatform.com'

oauth_endpoint = sp_url + '/o/'
authorization_base_url = oauth_endpoint + 'authorize/'
token_url = oauth_endpoint + 'token/'

# This allows us to use a plain HTTP callback
os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = "1"


def single_company(sp_client, id_of_company="1"):
    req_data = {'company_id': id_of_company}
    data_url = sp_url + '/api/data/'
    data_to_ouput = sp_client.post(data_url, data=req_data)
    return data_to_ouput.json()


def multi_company(sp_client, ids_of_company="1,2,3"):
    req_data = {'port_ids': ids_of_company}  # Max of 1000
    multi_data_url = sp_url + '/api/portfolio_data/'
    data_to_ouput = sp_client.post(multi_data_url, data=req_data)
    return data_to_ouput.json()


def write_multiple_data(json_obj):

    company_information = json_obj['company_data']

    today = date.today()
    todays_date = today.strftime("%Y-%m-%d")
    file_name = f'generated_files/{todays_date}_headless_output.csv'

    with open(file_name, 'w') as data_file:
        header = company_information[0].keys()
        csv_writer = csv.DictWriter(data_file, fieldnames=header)
        csv_writer.writeheader()

        for comp in company_information:
            csv_writer.writerow(comp)


def main():
    print('1. Authorisation Code Flow')
    print('2. Client Credentials Flow')
    selected_option = input('[1/2]: ')

    if selected_option == "1":
        token = get_auth_code_token(client_id, client_secret, authorization_base_url, token_url)
    elif selected_option == "2":
        token = get_token_client_credentials_flow(client_id, client_secret, token_url)
    else:
        print('Invalid option selected.')
        return

    sp_client = OAuth2Session(client_id, token=token)

    single_company_data = single_company(sp_client, "5609")
    print(single_company_data)

    multi_companies_string = "5609,16450"
    multiple_company_data = multi_company(sp_client, multi_companies_string)
    write_multiple_data(multiple_company_data)
    print(multiple_company_data)


if __name__ == '__main__':
    main()
