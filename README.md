# Sustainable Platform API Client

This repository has some examples on how to interact with the Sustainable Platform API
* app.py (Authorization Code only)
  * Flask example.
* headless.py (Auth Code / Client Credentials)
* headless_full_data.py (Auth Code / Client Credentials)

## Requirements

* Python 3.4+ (Tested with Python 3.8)
* Flask
* requests_oauthlib
* tqdm

## How to Run

First thing required is an account with [Sustainable Platform](https://sustainableplatform.com) which allows API access.

You will then need to update details in the client_credentials.json file with the details provided via email, in a similar format to client_credentials.json.default

You will also need to ensure "this_server_ip" is correct for your machine, or you can use the default of 0.0.0.0 - I have on occasion found 0.0.0.0 does not work as expected however.

Normally at the start of integration, the API is set up with the callback pointing to "localhost:5000", if you require a different endpoint, please contact the platform team.


After installing the requirements and  you should be able to start the application with:
```
python app.py
```

or alternatively if you prefer pipenv you can use:
```
pipenv install
pipenv run python app.py
```

## Questions

If you have questions or problems with this application you can direct questions to either your account manager or to the platform team directly.
