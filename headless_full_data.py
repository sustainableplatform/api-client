import ast
import csv
import json
import os
from datetime import datetime

from requests_oauthlib import OAuth2Session
from tqdm import tqdm

from helpers import get_auth_code_token, get_token_client_credentials_flow

from helpers.api_requests import (
    get_all_markets,
    get_all_available_fields,
    get_market_ids,
    multi_company,
)


with open('client_credentials.json', 'r') as json_file:
    data = json.load(json_file)
    client_id = data[0]["client_id"]
    client_secret = data[0]["client_secret"]

# sp_url = 'http://192.168.0.2:8002'
sp_url = 'https://api.sustainableplatform.com'

oauth_endpoint = sp_url + '/o/'
authorization_base_url = oauth_endpoint + 'authorize/'
token_url = oauth_endpoint + 'token/'

# This allows us to use a plain HTTP callback - if required
os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = "1"


def get_chunked_ids(sp_client, market):
    ids = get_market_ids(sp_url, sp_client, market)

    id_list = ast.literal_eval(f'{ids}')

    total_number_of_companies = len(id_list)
    ids_seen = []
    chunked_ids = []
    for id in id_list:
        ids_seen.append(id)
        if len(ids_seen) == 1000:
            string_of_ids = ','.join([str(id) for id in ids_seen])
            chunked_ids.append(string_of_ids)
            ids_seen = []
    string_of_ids = ','.join([str(id) for id in ids_seen])
    if string_of_ids:
        chunked_ids.append(string_of_ids)

    return chunked_ids, total_number_of_companies


def construct_dictionary_market_list(sp_client):
    all_markets = get_all_markets(sp_url, sp_client)

    market_to_id_dictionary = {}
    total_number_of_companies = 0
    for market in tqdm(all_markets, desc='Market Dictionary Construction', unit="market"):
        chunked_ids, market_total_number_of_companies = get_chunked_ids(sp_client, market)
        if market_total_number_of_companies > 0:
            total_number_of_companies += market_total_number_of_companies
            market_to_id_dictionary[market] = (chunked_ids, market_total_number_of_companies)
    return market_to_id_dictionary, total_number_of_companies


def get_company_information(sp_client):

    market_to_id_dictionary, total_number_of_companies = construct_dictionary_market_list(sp_client)
    resultant_data = []

    top_level_progress_bar = tqdm(desc="Total companies fetched", total=total_number_of_companies, unit="company")
    market_progress_bar = tqdm(market_to_id_dictionary.keys(), desc="Markets Processed", unit="market")
    for market in market_to_id_dictionary.keys():
        chunked_ids, market_id_count = market_to_id_dictionary[market]
        description_message = f'Requests for {market} ({market_id_count} companies)'
        for id_string in tqdm(chunked_ids, desc=description_message, leave=False):
            json_result = multi_company(sp_url, sp_client, id_string)
            resultant_data.append(json_result['company_data'])
        top_level_progress_bar.update(market_id_count)
        market_progress_bar.update(1)

    return resultant_data


def write_file(sp_client, all_company_data_by_market):
    fields = get_all_available_fields(sp_url, sp_client)

    todays_date = datetime.today().strftime('%Y-%m-%d')
    outname = f'generated_files/{todays_date}_SP_All_Data.csv'

    with open(outname, 'w', newline='') as full_data_file:
        csv_file_writer = csv.DictWriter(full_data_file, fieldnames=fields)
        csv_file_writer.writeheader()
        for market_companies in all_company_data_by_market:
            for comp in market_companies:
                csv_file_writer.writerow(comp)
    print('\n\n\n\n')
    print('File written')


def main():
    print('1. Authorisation Code Flow')
    print('2. Client Credentials Flow')
    selected_option = input('[1/2]: ')

    if selected_option == "1":
        token = get_auth_code_token(client_id, client_secret, authorization_base_url, token_url)
    elif selected_option == "2":
        token = get_token_client_credentials_flow(client_id, client_secret, token_url)
    else:
        print('Invalid option selected.')
        return

    sp_client = OAuth2Session(client_id, token=token)
    all_company_data_by_market = get_company_information(sp_client)

    write_file(sp_client, all_company_data_by_market)


if __name__ == '__main__':
    main()
